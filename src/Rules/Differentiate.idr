module Rules.Differentiate

import Decidable.Equality
import Data.Vect
import Common.Vect
import Term.Domain
import Term.Term
import Term.DecEq
import RuleFramework.Step
import RuleFramework.Rule
import RuleFramework.Combinators
import Rules.Simplify

%default total

record Monomial where
  constructor MkMonomial
  var : VarTerm R
  exp : Term R

ForStep Monomial where
  forStep m = let (x !! _) = var m in StepTerm $ x^(exp m)

record Chain where
  constructor MkChain
  outer : FuncTerm R R
  inner : FuncTerm R R

ForStep Chain where
  forStep chain = StepList [ LabeledPart "outer" (forStep (outer chain))
                           , LabeledPart "inner" (forStep (inner chain))
                           ]

mutual
  export
  differentiate : Rule (FuncTerm R R) (FuncTerm R R)
  differentiate = MkRule "differentiate" $ diffRules >> act simplifyFunc
    where
      differentiateNotConstant : RuleAction (FuncTerm R R) (FuncTerm R R)

      diffRules : RuleAction (FuncTerm R R) (FuncTerm R R)
      diffRules = act differentiateConstant || differentiateNotConstant

      differentiateNotConstant = do
        ((Func x def) !! _) <- Input

        case def of
             neg@(Minus _) =>
               Call (differentiateNegated x) (neg !! IsMinusPrf)

             y@(Var _) =>
               if x == (y !! IsVarPrf)
                  then let mon = MkMonomial x 1
                       in  Call differentiateByPowerRule mon
                  -- constant, so handled by previous rule
                  else Reject

             sum@(Sum _) =>
                Call (differentiateBySumRule x) (sum !! IsSumPrf)

             prod@(Prod xs) =>
               case splitConstFactors x (prod !! IsProdPrf) of
                    (Nothing, Nothing) => Throw PreconditionViolated
                    (Nothing, Just _) => Call (differentiateByProductRule x) (Prod xs !! IsProdPrf)
                    (Just _, Nothing) => Reject -- constant, so handled by previous rule
                    (Just const, Just vars) => Call (differentiateByFactorRule x) (("Factor", const), ("Rest", vars))

             (Power base exp) =>
               case (dependsOn x base, dependsOn x exp) of
                    (False, False) => Reject -- constant
                    (False, True) => Reject -- TODO: implement later
                    (True, False) =>
                      case base of
                           (Var _) => let mon = MkMonomial x exp
                                      in  Call differentiateByPowerRule mon
                           _ => let outer = x |-> (asTerm x)^exp
                                    inner = x |-> base
                                    chain = MkChain outer inner
                                in  Call differentiateByChainRule chain
                    (True, True) => Reject -- stuff like x^x, might need special handling later

             _ => Reject

  differentiateConstant : Rule (FuncTerm R R) (FuncTerm R R)
  differentiateConstant = MkRule "differentiateConstant" $ do
    ((Func x def) !! _) <- Input
    if not (dependsOn x def)
       then Return (x |-> 0)
       else Reject

  differentiateNegated : VarTerm R -> Rule (MinusTerm R) (FuncTerm R R)
  differentiateNegated x = MkRule "differentiateNegated" $ do
    ((Minus t) !! _) <- Input
    ((Func _ diff) !! _) <- Call differentiate (x |-> t)
    Return (x |-> (Minus diff))

  differentiateByPowerRule : Rule Monomial (FuncTerm R R)
  differentiateByPowerRule = MkRule "differentiateByPowerRule" $ do
    MkMonomial x exp <- Input
    let newFn = case exp of
                     -- in this case, we should have simplified before, but this behaviour will still work
                     (TInt 0) => 0
                     (TInt 1) => 1
                     _ => exp * (asTerm x)^(exp - 1)
    Return (x |-> newFn)

  differentiateBySumRule : VarTerm R -> Rule (SumTerm R) (FuncTerm R R)
  differentiateBySumRule x = MkRule "differentiateBySumRule" $ do
    ((Sum xs) !! _) <- Input
    let summandFns = map (x |->) xs
    diffedFns <- onAll summandFns differentiate
    let diffedDefs = map getDef diffedFns
    Return (x |-> (Sum diffedDefs))

  differentiateByFactorRule : VarTerm R -> Rule ((String, Term R), (String, Term R)) (FuncTerm R R)
  differentiateByFactorRule x = MkRule "differentiateByFactorRule" $ do
    ((_, const), (_, varTerm)) <- Input
    if (dependsOn x const)
       then Reject
       else do
         ((Func _ diff) !! _) <- Call differentiate (x |-> varTerm)
         Return (x |-> (const * diff))

  differentiateByProductRule : VarTerm R -> Rule (ProdTerm R) (FuncTerm R R)
  differentiateByProductRule x = MkRule "differentiateByProductRule" $ do
    prod <- Input
    let (fd, gd) = unflatten prod
    let f = x |-> fd
    let g = x |-> gd
    f' <- Call differentiate f
    g' <- Call differentiate g
    Return (f' * g + f * g')
   where
     unflatten : ProdTerm domain -> (Term domain, Term domain)
     unflatten (Prod {prf} xs !! IsProdPrf) = case xs of
                                                   [] => absurd prf
                                                   (y :: []) => absurd prf
                                                   (y :: (z :: [])) => (y, z)
                                                   (y :: (z :: (w :: ws))) => (y, Prod (z :: w :: ws))

  differentiateByChainRule : Rule Chain (FuncTerm R R)
  differentiateByChainRule = MkRule "differentiateByChainRule" $ do
    MkChain f g <- Input
    f' <- Call differentiate f
    g' <- Call differentiate g
    Return $ (f' . g) * g'

  -- TODO: maybe extract some (all?) of these functions so they're reusable

  splitConstFactors : {domain' : Domain} -> {domain : Domain} -> VarTerm domain' -> ProdTerm domain ->
                      (Maybe (Term domain), Maybe (Term domain))
  splitConstFactors x ((Prod factors) !! _) = let (consts, vars) = split [] [] (toList factors)
                                              in  (merge consts, merge vars)
    where
      TL : Domain -> Type
      TL domain = List (Term domain)

      split : (consts : TL domain) -> (vars : TL domain) -> (factors : TL domain) -> (TL domain, TL domain)
      split consts vars [] = (reverse consts, reverse vars)
      split consts vars (t :: ts) = if dependsOn x t
                                       then split consts (t :: vars) ts
                                       else split (t :: consts) vars ts

      merge : TL domain -> Maybe (Term domain)
      merge terms = let (n ** termVec) = toVec terms
                    in  case isGTE n 2 of
                             Yes prf => Just (Prod termVec)
                             No _ => case decEq n 1 of
                                          (Yes Refl) => case termVec of
                                                             ([t]) => Just t
                                          (No _) => Nothing

  getDef : FuncTerm domain codomain -> Term codomain
  getDef ((Func _ def) !! IsFuncPrf) = def

  (+) : FuncTerm domain codomain -> FuncTerm domain codomain -> FuncTerm domain codomain
  (+) ((Func x def) !! IsFuncPrf) ((Func y def') !! IsFuncPrf) =
    -- TODO: if y != x, replace y with x in def' (or find new variable)
    x |-> def + def'

  (*) : FuncTerm domain codomain -> FuncTerm domain codomain -> FuncTerm domain codomain
  (*) ((Func x def) !! IsFuncPrf) ((Func y def') !! IsFuncPrf) =
    -- TODO: if y != x, replace y with x in def' (or find new variable)
    x |-> def * def'

  (.) : FuncTerm R R -> FuncTerm R R -> FuncTerm R R

  -- assumes that both functions are functions of x
  -- TODO: change variables if necessary!
  (.) ((Func x def) !! IsFuncPrf) ((Func _ repl) !! IsFuncPrf) = x |-> replace def
   where
     replace : Term R -> Term R
     replace (TInt n) = TInt n
     replace (Minus t) = Minus (replace t)
     replace y@(Var _) = if (asTerm x) == y then repl else y
     replace (Sum xs) = Sum (map (assert_total replace) xs)
     replace (Prod xs) = Prod (map (assert_total replace) xs)
     replace (Power base exp) = Power (replace base) (replace exp)

  dependsOn : {domain : Domain} -> {domain' : Domain} -> VarTerm domain -> Term domain' -> Bool
  dependsOn x (TInt _) = False
  dependsOn x (Minus t) = dependsOn x t
  dependsOn (x !! _) y@(Var _) = case (assert_total decEq) domain domain' of
                                      Yes Refl => x == y
                                      No _ => False
  dependsOn x (Sum xs) = any ((assert_total dependsOn) x) xs
  dependsOn x (Prod xs) = any ((assert_total dependsOn) x) xs
  dependsOn x (Power base exp) = dependsOn x base || dependsOn x exp
  dependsOn x (Func y defn) = if sameVariable x y
                                 then False -- x is not free in defn, but bound by the function definition
                                 else dependsOn x defn
    where
      sameVariable : {domain : Domain} -> {domain' : Domain} -> VarTerm domain -> VarTerm domain' -> Bool
      sameVariable {domain} {domain'} x y = case (assert_total decEq) domain domain' of
                                                 Yes Refl => x == y
                                                 No _ => False
