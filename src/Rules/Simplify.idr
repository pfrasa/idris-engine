module Rules.Simplify

import Data.Nat
import Data.List
import Data.List1
import Data.Vect
import Common.Vect
import Term.Domain
import Term.Term
import Term.DecEq
import Term.Ord
import RuleFramework.Step
import RuleFramework.Rule
import RuleFramework.Combinators

%default total

mutual
  export
  simplifyScalar : Rule (Term (Scalar r)) (Term (Scalar r))
  simplifyScalar = MkRule "simplify" $
                   recursively flatten
                   >> act groupTermsInSumRec
                   >> act cancelTrivialExponentsRec
                   >> act multiplyNumbersRec
                   >> act addNumbersRec
                   >> act cancelZeroInSumRec
                   >> act resolveDoubleMinus
                   -- >> cancelOneInProducts

  export
  simplifyFunc : {codomain: _} -> Rule (FuncTerm domain codomain) (FuncTerm domain codomain)
  simplifyFunc = MkRule "simplify" $ do
    (Func x defn !! _) <- Input
    case codomain of
         (Scalar r) => do
           defn' <- Call simplifyScalar defn
           Return $ (Func x defn' !! IsFuncPrf)
         _ => Reject

  flatten : RuleAction (Term (Scalar r)) (Term (Scalar r))
  flatten = do
    input <- Input
    Return $ flatten' input
   where
     mutual
       flatten' : Term (Scalar r) -> Term (Scalar r)
       flatten' term = case term of
         (Sum {n = 0} {prf} xs) => absurd prf
         (Sum {n = (S 0)} {prf} xs) => absurd prf
         (Sum {n = (S (S k))} (x :: y :: ys)) => let (k ** (prf1, x')) = flatSummands x
                                                     (n ** (prf2, y')) = flatSummands y
                                                     (m ** ys') = flatSummandsL ys
                                                     prf = lengthPrf prf1 prf2 m
                                                 in  Sum (x' ++ y' ++ ys')
         (Prod {n = 0} {prf} xs) => absurd prf
         (Prod {n = (S 0)} {prf} xs) => absurd prf
         (Prod {n = (S (S k))} (x :: y :: ys)) => let (k ** (prf1, x')) = flatFactors x
                                                      (n ** (prf2, y')) = flatFactors y
                                                      (m ** ys') = flatFactorsL ys
                                                      prf = lengthPrf prf1 prf2 m
                                                  in  Prod (x' ++ y' ++ ys')
         _ => term

       flatSummands : Term (Scalar r) -> (n ** (GTE n 1, Vect n (Term (Scalar r))))
       flatSummands (Sum {n} {prf} xs) = (n ** (lteSuccLeft prf, xs)) -- TODO: flatten more than one level deep
       flatSummands t = (1 ** (reflexive, [t]))

       flatSummandsL : Vect n (Term (Scalar r)) -> (m ** Vect m (Term (Scalar r)))
       flatSummandsL xs = let xs' = map flatSummands xs
                          in  foldr (\(_ ** (_, xs'')), (_ ** acc) => (_ ** acc ++ xs'')) (0 ** []) xs'

       flatFactors : Term (Scalar r) -> (n ** (GTE n 1, Vect n (Term (Scalar r))))
       flatFactors (Prod {n} {prf} xs) = (n ** (lteSuccLeft prf, xs)) -- TODO: flatten more than one level deep
       flatFactors t = (1 ** (reflexive, [t]))

       flatFactorsL : Vect n (Term (Scalar r)) -> (m ** Vect m (Term (Scalar r)))
       flatFactorsL xs = let xs' = map flatFactors xs
                         in  foldr (\(_ ** (_, xs'')), (_ ** acc) => (_ ** acc ++ xs'')) (0 ** []) xs'

       lengthPrf : {k, n : _} -> GTE k 1 -> GTE n 1 -> (m : Nat) -> GTE (k + (n + m)) 2
       lengthPrf prf1 prf2 m = extend lengthPrf2
         where
           extend : GTE (k + n) 2 -> GTE (k + (n + m)) 2
           extend prf = let prf' = lteAddRight (k + n)
                        in  rewrite plusAssociative k n m
                        in  transitive prf prf'

           lengthPrf2 : GTE (k + n) 2
           lengthPrf2 = case k of
                             0 => absurd prf1
                             (S k') => case n of
                                            0 => absurd prf2
                                            (S n') => rewrite sym (plusSuccRightSucc k' n')
                                                      in LTESucc (LTESucc LTEZero)

  cancelTrivialExponentsRec : Rule (Term (Scalar r)) (Term (Scalar r))
  cancelTrivialExponentsRec = MkRule "cancelTrivialExponents" $ recursively cancelTrivialExponents

  cancelTrivialExponents : Rule (Term (Scalar r)) (Term (Scalar r))
  cancelTrivialExponents = MkRule "cancelTrivialExponents" $ do
    input <- Input
    case input of
         (Power base (TInt 0)) => Return 1
         (Power base (TInt 1)) => Return base
         _ => Reject

  multiplyNumbersRec : Rule (Term (Scalar r)) (Term (Scalar r))
  multiplyNumbersRec = MkRule "multiplyNumbers" $ recursively multiplyNumbers'
    where
      multiplyNumbers' : RuleAction (Term (Scalar r)) (Term (Scalar r))
      multiplyNumbers' = do
        term <- Input
        case term of
             (Prod xs) => Call multiplyNumbers (Prod xs !! IsProdPrf)
             _ => Reject

  multiplyNumbers : Rule (ProdTerm (Scalar r)) (Term (Scalar r))
  multiplyNumbers = MkRule "multiplyNumbers" $ do
    (Prod xs !! _) <- Input
    case splitNumsFromRest (toList xs) of
         ([], rest) => Return $ generalizedProd rest
         (ns, []) => Return $ mult ns
         (ns, rest) => Return $ (mult ns) * (generalizedProd rest)
   where
     mult : List Integer -> Term (Scalar r)
     mult xs = TInt $ foldr (*) 1 xs

  addNumbersRec : Rule (Term (Scalar r)) (Term (Scalar r))
  addNumbersRec = MkRule "addNumbers" $ recursively addNumbers'
    where
      addNumbers' : RuleAction (Term (Scalar r)) (Term (Scalar r))
      addNumbers' = do
        term <- Input
        case term of
             (Sum xs) => Call addNumbers (Sum xs !! IsSumPrf)
             _ => Reject

  addNumbers : Rule (SumTerm (Scalar r)) (Term (Scalar r))
  addNumbers = MkRule "addNumbers" $ do
    (Sum xs !! _) <- Input
    case getAllNums xs of
         Nothing => Reject
         Just nums => Return $ TInt $ sum nums

  cancelZeroInSumRec : Rule (Term (Scalar r)) (Term (Scalar r))
  cancelZeroInSumRec = MkRule "cancelZeroInSum" $ recursively cancelZeroInSum

  cancelZeroInSum : Rule (Term (Scalar r)) (Term (Scalar r))
  cancelZeroInSum = MkRule "cancelZeroInSum" $ do
    term <- Input
    case term of
         Sum xs =>
           let (n ** xs') = filter (/= 0) xs
           in  case n of
                    0 => Return 0
                    (S 0) => Return $ head xs'
                    (S (S k)) => Return $ Sum xs'
         _ => Reject

  resolveDoubleMinus : Rule (Term (Scalar r)) (Term (Scalar r))
  resolveDoubleMinus = MkRule "resolveDoubleMinus" $ recursively $ do
    term <- Input
    case term of
         (Minus (Minus x)) => Return x
         _ => Reject

  groupTermsInSumRec : Rule (Term (Scalar r)) (Term (Scalar r))
  groupTermsInSumRec = MkRule "groupTermsInSum" $ recursively groupTermsInSum

  groupTermsInSum : Rule (Term (Scalar r)) (Term (Scalar r))
  groupTermsInSum = MkRule "groupTermsInSum" $ do
    term <- Input
    case term of
         Sum xs => do
           let grouped = groupTermsOfSum (toList xs)
           simplified <- onAll grouped addNumbersRec
           Return $ generalizedSum simplified
         _ => Reject

  getAllNums : Vect n (Term (Scalar r)) -> Maybe (Vect n Integer)
  getAllNums [] = Just []
  getAllNums ((TInt n) :: xs) = let tail = getAllNums xs in map (n ::) tail
  getAllNums ((Minus (TInt n)) :: xs) = let tail = getAllNums xs in map (-n ::) tail
  getAllNums (_ :: xs) = Nothing

  splitNumsFromRest : List (Term (Scalar r)) -> (List Integer, List (Term (Scalar r)))
  splitNumsFromRest [] = ([], [])
  splitNumsFromRest (x :: xs) = let (nums, other) = splitNumsFromRest xs
                                in  case x of
                                         (TInt n) => (n :: nums, other)
                                         (Minus (TInt n)) => ((-n) :: nums, other)
                                         _ => (nums, x :: other)

  groupTermsOfSum : List (Term (Scalar r)) -> List (Term (Scalar r))
  groupTermsOfSum terms = let factoredTerms = map splitFactors terms
                              groups = groupAllWith (sort . snd) factoredTerms
                              prodGroups = map (map resolveProdInPair) groups
                              combined = map (foldr1 (\el, acc => (fst acc + fst el, snd el))) prodGroups
                          in  map joinPair combined
   where
     mutual
       resolveProdInPair : (List (Term (Scalar r)), List (Term (Scalar r))) -> (Term (Scalar r), Term (Scalar r))
       resolveProdInPair (fst, snd) = (generalizedProd fst, generalizedProd snd)

       joinPair : (Term (Scalar r), Term (Scalar r)) -> Term (Scalar r)
       joinPair (fst, snd) = if fst == 1 then snd else if snd == 1 then fst else fst * snd

       splitFactors : Term domain -> (List (Term domain), List (Term domain))
       splitFactors t = splitToPair (\factor => case factor of
                                                     TInt x => True
                                                     _ => False) (getFactors t)

       splitToPair : (a -> Bool) -> List a -> (List a, List a)
       splitToPair f [] = ([], [])
       splitToPair f (x :: xs) = let rec = splitToPair f xs
                                 in  if f x then (x :: fst rec, snd rec) else (fst rec, x :: snd rec)

       getFactors : Term domain -> List (Term domain)
       getFactors (Prod xs) = toList xs
       getFactors x = [x]

  generalizedSum : List (Term (Scalar r)) -> Term (Scalar r)
  generalizedSum [] = 0
  generalizedSum (x :: []) = x
  generalizedSum (x :: y :: ys) = let (_ ** tail) = toVec ys in Sum (x :: y :: tail)

  generalizedProd : List (Term (Scalar r)) -> Term (Scalar r)
  generalizedProd [] = 1
  generalizedProd (x :: []) = x
  generalizedProd (x :: y :: ys) = let (_ ** tail) = toVec ys in Prod (x :: y :: tail)

  -- cancelOneInProducts : Rule (Term domain) (Term domain)
