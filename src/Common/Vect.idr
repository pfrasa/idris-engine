module Common.Vect

import Data.Vect

export
toVec : List a -> (n ** Vect n a)
toVec [] = (_ ** [])
toVec (y :: ys) = let (_ ** ys') = toVec ys in (_ ** y :: ys')
