module Term.VarBindings

import Term.Term
import Term.Domain

%default total

public export
x : Term domain
x = Var "x"

public export
y : Term domain
y = Var "y"

public export
z : Term domain
z = Var "z"

public export
V : Term domain -> VarTerm domain
V t = case t of
           Var x => (Var x) !! IsVarPrf
           _ => (Var "invalid") !! IsVarPrf
