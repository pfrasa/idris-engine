module Term.DecEq

import Data.Nat
import Data.Vect
import Decidable.Equality
import Term.Domain
import Term.Term

%default total

-- this is gonna be really hard to maintain at scale :/
-- the sop library that is supposed to be able to auto-derive this don't seem to work properly
-- maybe worth researching elaborator reflection and how to do this via meta programming

-- for Domain

Uninhabited (Real = Complex) where
  uninhabited Refl impossible

public export
DecEq Ring where
  decEq Real Real = Yes Refl
  decEq Complex Complex = Yes Refl
  decEq Real Complex = No (\ass => absurd ass)
  decEq Complex Real = No (\ass => absurd (sym ass))

public export
Eq Ring where
  Real == Real = True
  Complex == Complex = True
  _ == _ = False

scalar_inj : Scalar r = Scalar r' -> r = r'
scalar_inj Refl = Refl

module_inj : Module r n = Module r' m -> (r = r', n = m)
module_inj Refl = (Refl, Refl)

matrix_inj : Matrix r m n = Matrix r' m' n' -> (r = r', m = m', n = n')
matrix_inj Refl = (Refl, Refl, Refl)

func_inj : d ==> c = d' ==> c' -> (d = d', c = c')
func_inj Refl = (Refl, Refl)

Uninhabited (Scalar r = Module r' n) where
  uninhabited Refl impossible

Uninhabited (Scalar r = Matrix r' n m) where
  uninhabited Refl impossible

Uninhabited (Scalar r = d ==> d') where
  uninhabited Refl impossible

Uninhabited (Module r n = Matrix r' m k) where
  uninhabited Refl impossible

Uninhabited (Module r n = d ==> d') where
  uninhabited Refl impossible

Uninhabited (Matrix r n m = d ==> d') where
  uninhabited Refl impossible

public export
DecEq Domain where
  decEq (Scalar r) (Scalar r') =
    case decEq r r' of
         Yes prf => rewrite prf in Yes Refl
         No contra => No (\ass => contra (scalar_inj ass))

  decEq (Module r k) (Module r' j) =
    case decEq r r' of
         Yes prf =>
           case decEq k j of
                Yes prf' => Yes $ rewrite prf in rewrite prf' in Refl
                No contra => No (\ass => let (_, neq) = module_inj ass in contra neq)
         No contra => No (\ass => let (req, _) = module_inj ass in contra req)

  decEq (Matrix r rows cols) (Matrix r' rows' cols') =
    case decEq r r' of
         Yes prf =>
           case decEq rows rows' of
                Yes prf' =>
                  case decEq cols cols' of
                       Yes prf'' => Yes $ rewrite prf in rewrite prf' in rewrite prf'' in Refl
                       No contra => No (\ass => let (_, _, coleq) = matrix_inj ass in contra coleq)
                No contra => No (\ass => let (_, roweq, _) = matrix_inj ass in contra roweq)
         No contra => No (\ass => let (req, _) = matrix_inj ass in contra req)

  decEq (d ==> c) (d' ==> c') =
    case decEq d d' of
         Yes prf =>
           case decEq c c' of
                Yes prf' => Yes $ rewrite prf in rewrite prf' in Refl
                No contra => No (\ass => let (_, ceq) = func_inj ass in contra ceq)
         No contra => No (\ass => let (deq, _) = func_inj ass in contra deq)

  decEq (Scalar r) (Module r' k) = No (\ass => absurd ass)
  decEq (Scalar r) (Matrix r' rows cols) = No (\ass => absurd ass)
  decEq (Scalar r) (domain ==> codomain) = No (\ass => absurd ass)
  decEq (Module r k) (Scalar r') = No (\ass => absurd (sym ass))
  decEq (Module r k) (Matrix r' rows cols) = No (\ass => absurd ass)
  decEq (Module r k) (domain ==> codomain) = No (\ass => absurd ass)
  decEq (Matrix r rows cols) (Scalar r') = No (\ass => absurd (sym ass))
  decEq (Matrix r rows cols) (Module r' k) = No (\ass => absurd (sym ass))
  decEq (Matrix r rows cols) (domain ==> codomain) = No (\ass => absurd ass)
  decEq (domain ==> codomain) (Scalar r) = No (\ass => absurd (sym ass))
  decEq (domain ==> codomain) (Module r k) = No (\ass => absurd (sym ass))
  decEq (domain ==> codomain) (Matrix r rows cols) = No (\ass => absurd (sym ass))

public export
Eq Domain where
  (==) d1 d2 = case decEq d1 d2 of
                    Yes _ => True
                    No _ => False

-- for Term

Uninhabited (TInt n = Minus t) where
  uninhabited Refl impossible

Uninhabited (TInt n = Var s) where
  uninhabited Refl impossible

(\x => GT x 1) n => Uninhabited (TInt m = Sum {n} xs) where
  uninhabited Refl impossible

(\x => GT x 1) n => Uninhabited (TInt m = Prod {n} xs) where
  uninhabited Refl impossible

Uninhabited (TInt n = Power base exp) where
  uninhabited Refl impossible

Uninhabited (TInt n = Func x defn) where
  uninhabited Refl impossible

Uninhabited (Minus t = Var s) where
  uninhabited Refl impossible

(\x => GT x 1) n => Uninhabited (Minus t = Sum {n} xs) where
  uninhabited Refl impossible

(\x => GT x 1) n => Uninhabited (Minus t = Prod {n} xs) where
  uninhabited Refl impossible

Uninhabited (Minus t = Power base exp) where
  uninhabited Refl impossible

Uninhabited (Minus t = Func x defn) where
  uninhabited Refl impossible

(\x => GT x 1) n => Uninhabited (Var s = Sum {n} xs) where
  uninhabited Refl impossible

(\x => GT x 1) n => Uninhabited (Var s = Prod {n} xs) where
  uninhabited Refl impossible

Uninhabited (Var s = Power base exp) where
  uninhabited Refl impossible

Uninhabited (Var s = Func x defn) where
  uninhabited Refl impossible

(\x => GT x 1) n => Uninhabited (Sum {n} xs = Power base exp) where
  uninhabited Refl impossible

(\x => GT x 1) n => Uninhabited (Sum {n} xs = Func x defn) where
  uninhabited Refl impossible

(\x => GT x 1) n => Uninhabited (Prod {n} xs = Power base exp) where
  uninhabited Refl impossible

(\x => GT x 1) n => Uninhabited (Prod {n} xs = Func x defn) where
  uninhabited Refl impossible

Uninhabited (Power base exp = Func x defn) where
  uninhabited Refl impossible

tint_inj : TInt m = TInt n -> m = n
tint_inj Refl = Refl

minus_inj : Minus t = Minus t' -> t = t'
minus_inj Refl = Refl

var_inj : Var x = Var y -> x = y
var_inj Refl = Refl

sum_inj : {n : Nat} -> {prf, prf' : GTE n 2} -> {xs, ys : Vect n (Term domain)} -> Sum {n} {prf} xs = Sum {n} {prf=prf'} ys -> xs = ys
sum_inj Refl = Refl

prod_inj : {n : Nat} -> {prf, prf' : GTE n 2} -> {xs, ys : Vect n (Term domain)} -> Prod {n} {prf} xs = Prod {n} {prf=prf'} ys -> xs = ys
prod_inj Refl = Refl

power_inj : Power base exp = Power base' exp' -> (base = base', exp = exp')
power_inj Refl = (Refl, Refl)

func_term_inj : Func ((Var name) !! IsVarPrf) defn = Func ((Var name') !! IsVarPrf) defn' -> (name = name', defn = defn')
func_term_inj Refl = (Refl, Refl)

gte_same_prf : {n, m : Nat} -> (prf : GTE n m) -> (prf' : GTE n m) -> prf = prf'
gte_same_prf prf prf' =
  case prf of
       LTEZero => case prf' of
                       LTEZero => Refl
       (LTESucc x) =>
         case prf' of
              (LTESucc y) => rewrite gte_same_prf x y in Refl

public export
DecEq (Term domain) where
  decEq (TInt x) (TInt y) = case decEq x y of
                                 Yes prf => rewrite prf in Yes Refl
                                 No contra => No (\ass => contra (tint_inj ass))

  decEq (Minus t1) (Minus t2) = case decEq t1 t2 of
                                     Yes prf => rewrite prf in Yes Refl
                                     No contra => No (\ass => contra (minus_inj ass))

  decEq (Var x) (Var y) = case decEq x y of
                               Yes prf => rewrite prf in Yes Refl
                               No contra => No (\ass => contra (var_inj ass))

  decEq (Sum {n=n} {prf=prf1} xs) (Sum {n=m} {prf=prf2} ys) =
    case decEq n m of
         Yes prf =>
           case prf of
                Refl => case (assert_total decEq) xs ys of
                             Yes prf' => rewrite prf' in Yes (case gte_same_prf prf1 prf2 of
                                                                   Refl => Refl)
                             No contra => No (\ass => contra (sum_inj ass))
         No contra => No (\ass => case ass of
                                       Refl => contra Refl)

  decEq (Prod {n=n} {prf=prf1} xs) (Prod {n=m} {prf=prf2} ys) =
    case decEq n m of
         Yes prf =>
           case prf of
                Refl => case (assert_total decEq) xs ys of
                             Yes prf' => rewrite prf' in Yes (case gte_same_prf prf1 prf2 of
                                                                   Refl => Refl)
                             No contra => No (\ass => contra (prod_inj ass))
         No contra => No (\ass => case ass of
                                       Refl => contra Refl)

  decEq (Power base exp) (Power base' exp') =
    case decEq base base' of
         Yes prf => case decEq exp exp' of
                         Yes prf' => rewrite prf in rewrite prf' in Yes Refl
                         No contra => No (\ass => let (_, eeq) = power_inj ass in contra eeq)
         No contra => No (\ass => let (beq, _) = power_inj ass in contra beq)

  decEq (Func ((Var name) !! IsVarPrf) defn) (Func ((Var name') !! IsVarPrf) defn') =
    case decEq name name' of
         Yes prf => case decEq defn defn' of
                         Yes prf' => rewrite prf in rewrite prf' in Yes Refl
                         No contra => No (\ass => let (_, defneq) = func_term_inj ass in contra defneq)
         No contra => No (\ass => let (nameeq, _) = func_term_inj ass in contra nameeq)

  decEq (TInt x) (Minus t1) = No absurd
  decEq (TInt x) (Var y) = No absurd
  decEq (TInt x) (Sum xs) = No absurd
  decEq (TInt x) (Prod xs) = No absurd
  decEq (TInt x) (Power base exp) = No absurd
  decEq (Minus t1) (TInt x) = No (\ass => (absurd (sym ass)))
  decEq (Minus t1) (Var x) = No absurd
  decEq (Minus t1) (Sum xs) = No absurd
  decEq (Minus t1) (Prod xs) = No absurd
  decEq (Minus t1) (Power base exp) = No absurd
  decEq (Minus t1) (Func variable defn) = No absurd
  decEq (Var x) (TInt y) = No (\ass => (absurd (sym ass)))
  decEq (Var x) (Minus t1) = No (\ass => (absurd (sym ass)))
  decEq (Var x) (Sum xs) = No absurd
  decEq (Var x) (Prod xs) = No absurd
  decEq (Var x) (Power base exp) = No absurd
  decEq (Var x) (Func variable defn) = No absurd
  decEq (Sum xs) (TInt x) = No (\ass => (absurd (sym ass)))
  decEq (Sum xs) (Minus t1) = No (\ass => (absurd (sym ass)))
  decEq (Sum xs) (Var x) = No (\ass => (absurd (sym ass)))
  decEq (Sum xs) (Prod ys) = No (\ass => case ass of
                                              Refl impossible)
  decEq (Sum xs) (Power base exp) = No absurd
  decEq (Sum xs) (Func variable defn) = No absurd
  decEq (Prod xs) (TInt x) = No (\ass => absurd (sym ass))
  decEq (Prod xs) (Minus t1) = No (\ass => absurd (sym ass))
  decEq (Prod xs) (Var x) = No (\ass => absurd (sym ass))
  decEq (Prod xs) (Sum ys) = No (\ass => case ass of
                                              Refl impossible)
  decEq (Prod xs) (Power base exp) = No absurd
  decEq (Prod xs) (Func variable defn) = No absurd
  decEq (Power base exp) (TInt x) = No (\ass => absurd (sym ass))
  decEq (Power base exp) (Minus t1) = No (\ass => absurd (sym ass))
  decEq (Power base exp) (Var x) = No (\ass => absurd (sym ass))
  decEq (Power base exp) (Sum xs) = No (\ass => absurd (sym ass))
  decEq (Power base exp) (Prod xs) = No (\ass => absurd (sym ass))
  decEq (Func variable defn) (Minus t1) = No (\ass => absurd (sym ass))
  decEq (Func variable defn) (Var x) = No (\ass => absurd (sym ass))
  decEq (Func variable defn) (Sum xs) = No (\ass => absurd (sym ass))
  decEq (Func variable defn) (Prod xs) = No (\ass => absurd (sym ass))

public export
Eq (Term domain) where
  (==) t1 t2 = case decEq t1 t2 of
                    Yes _ => True
                    No _ => False

public export
Eq (Subterm (Term domain) pred) where
  (==) (t1 !! _) (t2 !! _) = t1 == t2

public export
DecEq (MinusTerm domain) where
  decEq ((Minus t) !! IsMinusPrf) ((Minus t') !! IsMinusPrf) =
    case decEq t t' of
         Yes prf => rewrite prf in Yes Refl
         No contra => No (\ass => case ass of
                                       Refl => contra Refl)

public export
DecEq (SumTerm domain) where
  decEq ((Sum {n} {prf=prf1} summands) !! IsSumPrf) ((Sum {n=m} {prf=prf2} summands') !! IsSumPrf) =
    case decEq n m of
         Yes prf => case prf of
                         Refl => case decEq summands summands' of
                                      Yes prf' => rewrite prf' in
                                        case gte_same_prf prf1 prf2 of
                                             Refl => Yes Refl
                                      No contra => No (\ass => case ass of
                                                                    Refl => contra Refl)
         No contra => No (\ass => case ass of
                                       Refl => contra Refl)

public export
DecEq (ProdTerm domain) where
  decEq ((Prod {n} {prf=prf1} factors) !! IsProdPrf) ((Prod {n=m} {prf=prf2} factors') !! IsProdPrf) =
    case decEq n m of
         Yes prf => case prf of
                         Refl => case decEq factors factors' of
                                      Yes prf' => rewrite prf' in
                                        case gte_same_prf prf1 prf2 of
                                             Refl => Yes Refl
                                      No contra => No (\ass => case ass of
                                                                    Refl => contra Refl)
         No contra => No (\ass => case ass of
                                       Refl => contra Refl)

public export
DecEq (PowerTerm domain) where
  decEq ((Power base exp) !! IsPowerPrf) ((Power base' exp') !! IsPowerPrf) =
    case decEq base base' of
         Yes prf => case decEq exp exp' of
                         Yes prf' => rewrite prf in rewrite prf' in Yes Refl
                         No contra => No (\ass => case ass of
                                                       Refl => contra Refl)
         No contra => No (\ass => case ass of
                                       Refl => contra Refl)

public export
DecEq (VarTerm domain) where
  decEq ((Var name) !! IsVarPrf) ((Var name') !! IsVarPrf) =
    case decEq name name' of
         Yes prf => rewrite prf in Yes Refl
         No contra => No (\ass => case ass of
                                       Refl => contra Refl)

public export
DecEq (FuncTerm d c) where
  decEq ((Func x def) !! IsFuncPrf) ((Func y def') !! IsFuncPrf) =
    case decEq x y of
         Yes prf => case decEq def def' of
                         Yes prf' => rewrite prf in rewrite prf' in Yes Refl
                         No contra => No (\ass => case ass of
                                                       Refl => contra Refl)
         No contra => No (\ass => case ass of
                                       Refl => contra Refl)
