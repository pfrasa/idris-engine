module Term.Ord

import Data.Vect
import Decidable.Equality
import Term.Domain
import Term.Term
import Term.DecEq

%default total

-- metaprogramming?

export
Ord (VarTerm domain) where
  ((Var x) !! IsVarPrf) < ((Var y) !! IsVarPrf) = x < y

export
Ord (Term domain) where
  (TInt x) < (TInt y) = x < y
  (TInt _) < _ = True
  (Minus _) < (TInt _) = False
  (Minus t1) < (Minus t2) = t1 < t2
  (Minus _) < _ = True
  (Var _) < (TInt _) = False
  (Var _) < (Minus _) = False
  (Var x) < (Var y) = x < y
  (Var _) < _ = True
  (Sum _) < (TInt _) = False
  (Sum _) < (Minus _) = False
  (Sum _) < (Var _) = False
  (Sum {n} xs) < (Sum {n=m} ys) = case decEq n m of
                                       Yes Refl => assert_total $ xs < ys
                                       No _ => n < m
  (Sum _) < _ = True
  (Prod _) < (TInt _) = False
  (Prod _) < (Minus _) = False
  (Prod _) < (Var _) = False
  (Prod _) < (Sum _) = False
  (Prod {n} xs) < (Prod {n=m} ys) = case decEq n m of
                                         Yes Refl => assert_total $ xs < ys
                                         No _ => n < m
  (Prod xs) < _ = True
  (Power _ _) < (TInt _) = False
  (Power _ _) < (Minus _) = False
  (Power _ _) < (Var _) = False
  (Power _ _) < (Sum _) = False
  (Power _ _) < (Prod _) = False
  (Power base exp) < (Power base' exp') = assert_total $ (base, exp) < (base', exp')
  (Func _ _) < (Minus _) = False
  (Func _ _) < (Var _) = False
  (Func _ _) < (Sum _) = False
  (Func _ _) < (Prod _) = False
  (Func x def) < (Func y def') = assert_total $ (x, def) < (y, def')
