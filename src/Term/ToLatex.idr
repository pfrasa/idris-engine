module Term.ToLatex

import Data.Nat
import Data.Vect
import Data.String
import Term.Domain
import Term.Term

%default total

mutual
  termToLatex : Term _ -> String
  termToLatex (TInt n) = show n
  termToLatex (Minus t1) = "-" ++ toLatexGroup t1 group1
  termToLatex (Var str) = str
  termToLatex (Sum xs) =
    let sub = mapToLatexGroup xs group1
    in  (unwords . toList) (intersperse " + " sub)
  termToLatex (Prod xs) =
    let sub = mapToLatexGroup xs group1
    in  (unwords . toList) (intersperse " \\cdot " sub)
  termToLatex (Power base exp) =
    toLatexGroup base group2 ++ "^{" ++ termToLatex exp ++ "}"
  termToLatex (Func (var !! _) defn) =
    "f(" ++ termToLatex var ++ ") = " ++ termToLatex defn

  toLatexGroup : Term a -> (Term a -> Bool) -> String
  toLatexGroup t group =
    let sub = termToLatex t
    in  if group t then "\\left(" ++ sub ++ "\\right)" else sub

  group1 : Term _ -> Bool
  group1 (Minus _) = True
  group1 (Sum _) = True
  group1 (Func _ _) = True
  group1 _ = False

  group2 : Term _ -> Bool
  group2 (Minus _) = True
  group2 (Sum _) = True
  group2 (Prod _) = True
  group2 (Power _ _) = True
  group2 (Func _ _) = True
  group2 _ = False

  mapToLatexGroup : Vect n (Term a) -> (Term a -> Bool) -> Vect n String
  mapToLatexGroup [] _ = []
  mapToLatexGroup (x :: xs) group =
    toLatexGroup x group :: mapToLatexGroup xs group

export
interface ToLatex a where
  toLatex : a -> String

export
ToLatex (Term domain) where
  toLatex = termToLatex

export
ToLatex (Subterm (Term domain) pred) where
  toLatex (term !! _ ) = termToLatex term
