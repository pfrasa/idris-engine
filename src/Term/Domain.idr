module Term.Domain

%default total

public export
data Ring = Real | Complex

%name Ring r,r'

infix 1 ==>

public export
data Domain : Type where
  Scalar : Ring -> Domain
  Module : Ring -> Nat -> Domain
  Matrix : Ring -> (rows: Nat) -> (cols: Nat) -> Domain
  (==>) : (domain : Domain) -> (codomain : Domain) -> Domain

%name Domain r,r'

public export
R : Domain
R = Scalar Real

public export
C : Domain
C = Scalar Complex
