module Term.Show

import Data.Nat
import Data.Vect
import Data.String
import Term.Domain
import Term.Term

%default total

-- debug output for terms
mutual
  mapDisplay : Vect n (Term _) -> Vect n String
  mapDisplay [] = []
  mapDisplay (x :: xs) = display x :: mapDisplay xs

  display : Term _ -> String
  display (TInt n) = show n
  display (Minus t) = "-(" ++ display t ++ ")"
  display (Var x) = x
  display (Sum xs) = let sub = mapDisplay xs in "(" ++ (unwords . toList) (intersperse " + " sub) ++ ")"
  display (Prod xs) = let sub = mapDisplay xs in (unwords . toList) (intersperse " * " sub)
  display (Power base exp) = "(" ++ display base ++ ")^(" ++ display exp ++ ")"
  display (Func (var !! _) defn) = display var ++ " -> " ++ display defn

export
Show (Term domain) where
  show = display

export
Show (Subterm (Term domain) pred) where
  show (term !! _) = show term
