module Term.Term

import Data.Nat
import Data.Vect
import Term.Domain

%default total

mutual
  -- term definitions
  public export
  data Term : Domain -> Type where
    TInt : Integer -> Term (Scalar ring)
    Minus : Term domain -> Term domain
    Var : String -> Term domain
    Sum : {n : Nat} -> {auto prf : GT n 1} -> Vect n (Term domain) -> Term domain
    Prod : {n : Nat} -> {auto prf : GT n 1} -> Vect n (Term domain) -> Term domain
    Power : (base : (Term (Scalar ring))) -> (exp : (Term (Scalar ring))) -> Term (Scalar ring)
    Func : (variable : VarTerm domain) -> (defn : Term codomain) -> Term (domain ==> codomain)

  %name Term t1,t2,t3

  -- term subtypes
  infix 0 !!

  -- use this custom type instead of built-in Subset because of type erasure issues
  public export
  data Subterm : (term : Type) -> (term -> Type) -> Type where
    (!!) : (term : t) -> pred' term -> Subterm t pred'

  public export
  data IsMinus : Term domain -> Type where
    IsMinusPrf : IsMinus (Minus t)

  public export
  MinusTerm : Domain -> Type
  MinusTerm domain = Subterm (Term domain) IsMinus

  public export
  data IsSum : Term domain -> Type where
    IsSumPrf : {n : Nat} -> {auto prf : GT n 1} -> {summands : Vect n (Term domain)} -> IsSum (Sum summands)

  public export
  SumTerm : Domain -> Type
  SumTerm domain = Subterm (Term domain) IsSum

  public export
  data IsProd : Term domain -> Type where
    IsProdPrf : {n : Nat} -> {auto prf : GT n 1} -> {factors : Vect n (Term domain)} -> IsProd (Prod factors)

  public export
  ProdTerm : Domain -> Type
  ProdTerm domain = Subterm (Term domain) IsProd

  public export
  data IsPower : Term domain -> Type where
    IsPowerPrf : IsPower (Power base exp)

  public export
  PowerTerm : Domain -> Type
  PowerTerm domain = Subterm (Term domain) IsPower

  public export
  data IsVar : Term domain -> Type where
    IsVarPrf : IsVar (Var name)

  public export
  VarTerm : Domain -> Type
  VarTerm domain = Subterm (Term domain) IsVar

  public export
  data IsFunc : Term domain -> Type where
    IsFuncPrf : IsFunc (Func x def)

  public export
  FuncTerm : Domain -> Domain -> Type
  FuncTerm domain codomain = Subterm (Term (domain ==> codomain)) IsFunc

export
asTerm : Subterm (Term domain) pred -> Term domain
asTerm (term !! _) = term

-- convenience functions for building terms
-- most of these functions have return type Term instead of one of the subtypes, so they can be easily composed

public export
fromInteger : (i: Integer) -> Term (Scalar ring)
fromInteger = TInt

lteSuccComm : GT n 0 -> GT (plus n 1) 1
lteSuccComm prf = rewrite plusCommutative n 1 in LTESucc prf

public export
(+) : Term domain -> Term domain -> Term domain
(+) t1 t2 = let (_ ** (subprf, head)) = flatten t1
                newprf = lteSuccComm subprf
                summands = head ++ [t2]
            in Sum summands
  where
    flatten : Term domain -> (n: Nat ** (GT n 0, Vect n (Term domain)))
    flatten (Sum {n} {prf} xs) = (n ** (lteSuccLeft prf, xs))
    flatten t = (1 ** (LTESucc LTEZero, [t]))

public export
(-) : Term domain -> Term domain -> Term domain
(-) t1 t2 = Sum [t1, Minus t2]

public export
(*) : Term domain -> Term domain -> Term domain
(*) t1 t2 = let (_ ** (subprf, head)) = flatten t1
                newprf = lteSuccComm subprf
                factors = head ++ [t2]
            in Prod factors
  where
    flatten : Term domain -> (n: Nat ** (GT n 0, Vect n (Term domain)))
    flatten (Prod {n} {prf} xs) = (n ** (lteSuccLeft prf, xs))
    flatten t = (1 ** (LTESucc LTEZero, [t]))

infixr 10 ^

public export
(^) : (base: (Term (Scalar ring))) -> (exp: (Term (Scalar ring))) -> Term (Scalar ring)
(^) base exp = Power base exp

infix 1 |->

public export
(|->) : (variable: VarTerm domain) -> (defn : Term codomain) -> FuncTerm domain codomain
(|->) variable defn = (Func variable defn) !! IsFuncPrf
