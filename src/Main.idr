module Main

import Data.String
import Data.Vect
import Language.JSON
import Term.Term
import Term.Domain
import Term.VarBindings
import Term.ToLatex
import RuleFramework.Step
import RuleFramework.Rule
import RuleFramework.Run
import Rules.Differentiate
import Rules.Simplify

%default total

func : FuncTerm R R
func = V x |-> x + 2

func2 : FuncTerm R R
func2 = V y |-> x

func4 : FuncTerm R R
func4 = V x |-> Minus (x + 2)

func5 : FuncTerm R R
func5 = V x |-> Minus (Minus (2 * x + 2 * y - 3 * x + 19))

func6 : FuncTerm R R
func6 = V x |-> x * x

func7 : FuncTerm R R
func7 = V x |-> x * (2 * x) + 3 * x * (y + 1) * 5 * x

func8 : FuncTerm R R
func8 = V x |-> 3*x^2 + 2*x + 2

func9 : FuncTerm R R
func9 = V x |-> 3*(x + 2)^3

func10 : FuncTerm R R
func10 = V x |-> 3*(2 * x^2 + 5 * x + 9)^3

funcs : List (FuncTerm R R)
funcs = [func, func2, func4, func5, func6, func7, func8]

partToJson : StepPart -> JSON
partToJson (StepTerm t) = JString $ toLatex t
partToJson (StepList xs) = JArray $ map (assert_total partToJson) xs
partToJson (LabeledPart label part) = JObject [("label", JString label), ("value", partToJson part)]

stepToJson : Step -> JSON
stepToJson (MkStep name input output substeps) =
  JObject [ ("name", JString name)
          , ("input", partToJson input)
          , ("output", partToJson output)
          , ("steps", JArray (map (assert_total stepToJson) substeps))
          ]

toJson : (ToLatex a, ToLatex b) => a -> RuleResult (b, Step) -> JSON
toJson input (OK (result, step)) = JObject [ ("input", JString (toLatex input))
                                           , ("result", JString (toLatex result))
                                           , ("steps", stepToJson step)
                                           ]
toJson _ NotApplicable = JObject [("error", JString "not_applicable")]
toJson _ (RuleError x) = JObject [("error", JString (show x))]

%default covering

processRequest : String -> PrimIO (Int, String)
processRequest idxString =
  case parsePositive idxString of
       Just idx => case inBounds idx funcs of
                        Yes _ => let f = index idx funcs
                                     result = run forever differentiate f
                                     json = toJson f result
                                 in  toPrim $ pure (200, format 2 json)
                        No _ => toPrim $ pure (400, "invalid index")
       Nothing => toPrim $ pure (400, "invalid index")

%foreign "node:lambda:(callback)=>{ const app = require('express')(); const cors = require('cors'); app.use(cors()); app.get('/diff/:idx', (req, res) => { const {a1, a2} = callback(req.params['idx'])(); res.status(a1); res.send(a2); });app.listen(8000); }"
mkServer : (String -> PrimIO (Int, String)) -> PrimIO ()

main : IO ()
main = primIO $ mkServer processRequest
