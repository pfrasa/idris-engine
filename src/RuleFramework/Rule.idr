module RuleFramework.Rule

import Term.Domain
import Term.Term
import RuleFramework.Step

%default total

public export
data RuleErrorType = PreconditionViolated | TooManyIterations

export
Show RuleErrorType where
  show PreconditionViolated = "PreconditionViolated"
  show TooManyIterations = "TooManyIteration"

public export
data RuleResult t = OK t | NotApplicable | RuleError RuleErrorType

export
Functor RuleResult where
  map f (OK x) = OK (f x)
  map f NotApplicable = NotApplicable
  map f (RuleError x) = RuleError x

export
Applicative RuleResult where
  (<*>) (OK f) x = map f x
  (<*>) NotApplicable _ = NotApplicable
  (<*>) (RuleError e) _ = RuleError e
  pure = OK

export
Monad RuleResult where
  (>>=) x f = join $ map f x
    where
      join : RuleResult (RuleResult b) -> RuleResult b
      join (OK r) = r
      join NotApplicable = NotApplicable
      join (RuleError e) = RuleError e

export
Show t => Show (RuleResult t) where
  show (OK x) = show x
  show NotApplicable = "NotApplicable"
  show (RuleError x) = show x

mutual
  public export
  data Rule : (input : Type) -> (output : Type) -> Type where
    MkRule : (ForStep input, ForStep output) =>
             (name : String) -> RuleAction input output -> Rule input output

  public export
  data RuleAction : (input : Type) -> (output : Type) -> Type where
    Debug : String -> RuleAction input input
    Input : RuleAction input input
    Call : (ForStep input', ForStep output) =>
           Inf (Rule input' output) -> input' -> RuleAction input output
    Return : output -> RuleAction input output
    Throw : RuleErrorType -> RuleAction input output
    Reject : RuleAction input output
    (||) : RuleAction input output
           -> RuleAction input output
           -> RuleAction input output
    (>>=) : RuleAction t1 t2
            -> (t2 -> RuleAction t2 t3)
            -> RuleAction t1 t3
