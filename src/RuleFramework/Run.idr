module RuleFramework.Run

import Debug.Trace
import RuleFramework.Step
import RuleFramework.Rule

%default total

public export
data Iterations = Done | More (Lazy Iterations)

export
iterations : Nat -> Iterations
iterations 0 = Done
iterations (S k) = More (iterations k)

partial export
forever : Iterations
forever = More forever

-- run rules with steps
export
run : Iterations -> Rule input output -> input -> RuleResult (output, Step)

runAction : Iterations -> RuleAction input output -> input -> RuleResult (output, List Step)
runAction Done _ _ = RuleError TooManyIterations
runAction _ (Debug s) input = trace s (OK (input, []))
runAction _ Input input = OK (input, [])
runAction (More it) (Call rule newInput) _ = map collect $ run it rule newInput
  where
    collect : (output, Step) -> (output, List Step)
    collect (output, step) = (output, [step])
runAction _ (Return output) _ = OK (output, [])
runAction _ (Throw error) _ = RuleError error
runAction _ Reject _ = NotApplicable
runAction it (act1 || act2) input = do
  let res1 = runAction it act1 input
  case res1 of
       NotApplicable => runAction it act2 input
       other => other
runAction it (act1 >>= cont) input = do
  (result1, steps1) <- runAction it act1 input
  (result2, steps2) <- runAction it (cont result1) result1
  OK (result2, steps1 ++ steps2)

run it (MkRule name action) input = map collect (runAction it action input)
  where
    collect : (output, List Step) -> (output, Step)
    collect (output, substeps) =
      let step = MkStep name (forStep input) (forStep output) substeps
      in  (output, step)

-- run rules without gathering steps
export
exec : Iterations -> Rule input output -> input -> RuleResult output
exec it rule input = map fst $ run it rule input
