module RuleFramework.Combinators

import Data.Vect
import Term.Domain
import Term.Term
import RuleFramework.Step
import RuleFramework.Rule

%default total

export
act : (ForStep a, ForStep b) => Rule a b -> RuleAction a b
act rule = do
  input <- Input
  Call rule input

export
(>>) : RuleAction t1 t2 -> RuleAction t2 t3 -> RuleAction t1 t3
(>>) a1 a2 = a1 >>= (\_ => a2)

export
onAll : Vect n input -> RuleAction input output -> RuleAction input' (Vect n output)
onAll [] _ = Return []
onAll (x :: xs) ra = Return x >> ra >> do
  fx <- Input
  fxs <- onAll xs ra
  Return (fx :: fxs)

namespace OnAllOnLists
  export
  onAll : List input -> RuleAction input output -> RuleAction input' (List output)
  onAll [] _ = Return []
  onAll (x :: xs) ra = Return x >> ra >> do
    fx <- Input
    fxs <- onAll xs ra
    Return (fx :: fxs)

namespace RuleOnAll
  export
  onAll : (ForStep input, ForStep output) =>
          Vect n input -> Rule input output -> RuleAction input' (Vect n output)
  onAll xs rule = onAll xs $ do
    input <- Input
    Call rule input

namespace RuleOnAllOnLists
  export
  onAll : (ForStep input, ForStep output) =>
          List input -> Rule input output -> RuleAction input' (List output)
  onAll xs rule = onAll xs $ do
    input <- Input
    Call rule input

export
id : RuleAction a a
id = do
  input <- Input
  Return input

export
try : RuleAction a a -> RuleAction a a
try ra = ra || id

export
recursively : RuleAction (Term domain) (Term domain) -> RuleAction (Term domain) (Term domain)
recursively ra = try ra >> recurse
 where
   recurse : RuleAction (Term domain) (Term domain)
   recurse = do
     rootResult <- Input
     case rootResult of
          (TInt _) => Return rootResult
          (Minus _) => Return rootResult
          (Var _) => Return rootResult
          (Sum xs) => onAll xs (assert_total (recursively ra)) >> do
            xs' <- Input
            Return (Sum xs')
          (Prod xs) => onAll xs (assert_total (recursively ra)) >> do
            xs' <- Input
            Return (Prod xs')
          (Power base exp) => Return base >> assert_total (recursively ra) >> do
            base' <- Input
            Return exp >> assert_total (recursively ra) >> do
              exp' <- Input
              Return $ Power base' exp'
          -- if ra is an action that transforms functions, it can't be applied on
          -- the function content too! (domain mismatch)
          (Func variable defn) => Return rootResult

namespace RecursivelyOnRule
  export
  recursively : Rule (Term domain) (Term domain) -> RuleAction (Term domain) (Term domain)
  recursively = recursively . act
