module RuleFramework.Step

import Data.Vect
import Term.Domain
import Term.Term

public export
data StepPart = StepTerm (Term domain)
              | StepList (List StepPart)
              | LabeledPart String StepPart

public export
interface ForStep t where
  forStep : t -> StepPart

public export
ForStep (Term domain) where
  forStep = StepTerm

public export
ForStep (Subterm (Term domain) pred) where
  forStep = StepTerm . asTerm

public export
ForStep a => ForStep (Vect n a) where
  forStep = StepList . (map forStep) . toList

public export
ForStep a => ForStep (List a) where
  forStep = StepList . (map forStep)

-- TODO: is this good design?
public export
(ForStep a, ForStep b) => ForStep ((String, a), (String, b)) where
  forStep ((s1, x), (s2, y)) = StepList [ LabeledPart s1 (forStep x)
                                        , LabeledPart s2 (forStep y)
                                        ]

public export
record Step where
  constructor MkStep
  name : String
  input : StepPart
  output : StepPart
  substeps : List Step
