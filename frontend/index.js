const examples = [
  {
    display: "f(x) = x + 2",
    url: "/diff/0"
  },
  {
    display: "f(y) = x",
    url: "/diff/1"
  },
  {
    display: "f(x) = -(x+2)",
    url: "/diff/2"
  },
  {
    display: "f(x) = -(-(2x + 2y - 3x + 19))",
    url: "/diff/3"
  },
  {
    display: "f(x) = x\\cdot x",
    url: "/diff/4"
  },
  {
    display: "f(x) = x\\cdot (2x) + 3x(y+1) \\cdot 5x",
    url: "/diff/5"
  },
  {
    display: "f(x) = 3x^2 + 2x + 2",
    url: "/diff/6"
  }
];

const root = document.getElementById("main");

const katexConfig = {
  delimiters: [
    { left: "$$", right: "$$", display: true },
    { left: "$", right: "$", display: false }
  ]
};

const renderMath = math => `\$${math}\$`;

const ExampleComponent = _ => {
  let showSolution = false;
  let solution = null;

  return {
    oncreate: vnode => {
      renderMathInElement(vnode.dom, katexConfig);
    },
    onupdate: vnode => {
      renderMathInElement(vnode.dom, katexConfig);
    },
    view: vnode => {
      const example = vnode.attrs.example;

      const item = showSolution ?
        [
          m("span", "Differentiate " + renderMath(example.display)),
          m("button", {
            onclick: () => {
              showSolution = false;
            },
            class: "solution-btn"
          }, "Hide Solution"),
          m(SolutionComponent, { solution })
        ] :
        [
          m("span", "Differentiate " + renderMath(example.display)),
          m("button", {
            onclick: (e) => {
              showSolution = true;

              if (!!solution) {
                e.redraw = true;
              } else {
                e.redraw = false;
                loadResult(example).then(response => {
                  solution = response;
                });
              }
            },
            class: "solution-btn"
          }, "Show Solution")
        ];

      return m("li", m("div", item));
    }
  };
};

const SolutionComponent = _ => {
  return {
    oncreate: vnode => {
      renderMathInElement(vnode.dom, katexConfig);
    },
    onupdate: vnode => {
      renderMathInElement(vnode.dom, katexConfig);
    },
    view: vnode => {
      const solution = vnode.attrs.solution;
      const steps = solution.steps.steps;

      return m("div", [
        m("h2", "Result"),
        m("span", renderMath(solution.result)),
        m(StepsComponent, { steps })
      ]);
    }
  };
};

const StepsComponent = _ => {
  let showSteps = false;

  return {
    view: vnode => {
      const steps = vnode.attrs.steps;

      if (steps.length > 0) {
        const inner = showSteps ?
          [
            m("button", {
              onclick: () => {
                showSteps = false;
              },
              class: "steps-btn"
            }, "Hide Steps"),
            m("ol", steps.map(step =>
              m(StepComponent, { step })
            ))
          ] :
          [
            m("button", {
              onclick: () => {
                showSteps = true;
              },
              class: "steps-btn"
            }, "Show Steps"),
          ];

        return [m("h3", "Steps"), m("div", inner)];
      } else {
        return []
      }
    }
  };
};

const StepComponent = _ => {
  return {
    oncreate: vnode => {
      renderMathInElement(vnode.dom, katexConfig);
    },
    onupdate: vnode => {
      renderMathInElement(vnode.dom, katexConfig);
    },
    view: vnode => {
      const step = vnode.attrs.step;

      return m("li", [
        m("h3", "Rule"),
        m("p", step.name),
        m("h3", "Input"),
        m(StepPartComponent, { part: step.input }),
        m("h3", "Output"),
        m(StepPartComponent, { part: step.output }),
        m(StepsComponent, { steps: step.steps })
      ]);
    }
  };
};

const StepPartComponent = _ => {
  return {
    oncreate: vnode => {
      renderMathInElement(vnode.dom, katexConfig);
    },
    onupdate: vnode => {
      renderMathInElement(vnode.dom, katexConfig);
    },
    view: vnode => {
      const part = vnode.attrs.part;

      if (typeof part === "string") {
        return [m("span", renderMath(part))];
      } else if (Array.isArray(part)) {
        return part.flatMap(subpart => m(StepPartComponent, { part: subpart }));
      } else {
        return [
          m("p",
            m("strong", `${part.label}: `),
            m("span", renderMath(part.value))
          )
        ];
      }
    }
  };
};

m.mount(root, {
  view: () => {
    return m("ul", examples.map(example => {
      return m(ExampleComponent, { example });
    }));
  }
});

const apiUrl = "http://localhost:8000"

const loadResult = example => {
  return m.request({
    method: "GET",
    url: `${apiUrl}${example.url}`
  })
};
